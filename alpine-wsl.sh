#!/bin/bash

function startmsg(){
    echo -e "\e[0Ksection_start:`date +%s`:dev_install[collapsed=true]\r\e[0K\e[1;34m ${1} \e[0m";

}
function endmsg(){
    echo -e "\e[0Ksection_end:`date +%s`:dev_install\r\e[0K";
}

startmsg "1/11 TEST"
    echo test
endmsg

# startmsg "1/11 init"
#     sudo dpkg --configure -a
#     sudo apt-get update
#     sudo apt-get -f install -y
#     sudo apt-get full-upgrade -y
#     # sudo apt-get install --reinstall ubuntu-desktop -y
#     sudo apt-get autoremove -y
#     sudo apt-get clean
# endmsg

# startmsg "2/11 add repos"
#     sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
#     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#     sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu jammy stable"
#     wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
#     sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
#     sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
#     rm -f packages.microsoft.gpg
#     sudo apt update
#     sudo apt upgrade -y
#     sudo apt autoremove -y
#     apt-cache policy docker-ce
# endmsg

# startmsg "3/11 INSTALL zip git vim curl zsh bash openssl"
#     sudo apt install -y zip git curl wget vim zsh bash openssl
# endmsg

# startmsg "4/11 INSTALL vscode"
#     sudo apt install -y code
# endmsg

# startmsg "5/11 INSTALL php"
#     sudo apt install -y php
# endmsg

# startmsg "6/11 INSTALL docker"
#     sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose
#     sudo apt remove apache2 -y
#     sudo apt autoremove -y
#     sudo apt clean
#     # ca-certificates gnupg lsb-release
#     sudo usermod -aG docker ${USER}
#     id -nG
# endmsg

# startmsg "7/11 install composer"
#     if [ ! -f /usr/local/bin/composer ];then
#         php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#         php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#         php composer-setup.php
#         php -r "unlink('composer-setup.php');"
#         sudo mv composer.phar /usr/local/bin/composer
#         sudo composer self-update
#         composer --version
#     fi
# endmsg

# startmsg "8/11 ohmyzsh & plugins"
#     if [ ! -d ~/.oh-my-zsh ]; then
#         sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --unattended
#     fi
#     if [ ! -d ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k ]; then
#         git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
#     fi
#     if [ ! -d ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh_carcheky ]; then
#         git clone --depth=1 https://gitlab.com/carcheky/zsh_carcheky.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh_carcheky
#     fi
# endmsg

# startmsg "9/11 install fonts"
#     mkdir ~/.fonts
#     cd ~/.fonts/
#     wget -q https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
#     wget -q https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
#     wget -q https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
#     wget -q https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
#     fc-cache -f -v
#     ls -la ~/.fonts
# endmsg

# startmsg "10/11 default zsh"
#     sudo usermod -s /bin/zsh ${USER}
#     sudo chsh -s $(which zsh)
#     cd
#     [ -f .zshrc ] && rm .zshrc
#     wget -q https://gitlab.com/carcheky/ubuntu-dev/-/raw/jammy/.zshrc
#     sed -i "s/home\/carcheky/home\/${USER}/g" ~/.zshrc
#     cat ~/.zshrc | grep plugins
#     cat ~/.zshrc | grep powerlevel
#     cat ~/.zshrc | grep .oh-my-zsh
# endmsg

# startmsg "11/11 ssh key"
#     if [ ! -d "~/.ssh" ]; then
#         mkdir ~/.ssh
#     fi
#     if [ ! -f "~/.ssh/id_rsa" ]; then
#         ssh-keygen -b 2048 -t rsa -f ~/.ssh/id_rsa -q -N "" ; 
#         ls -la ~/.ssh
#     fi
# endmsg

zsh
